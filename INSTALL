                          LEMONLDAP::NG INSTALLATION

Lemonldap::NG is a modular Web-SSO based on Apache::Session modules. It
simplifies the build of a protected area with a few changes in the application.
It manages both authentication and authorization and provides headers for
accounting. So you can have a full AAA protection.

See README file to known how it works.

----------------------
I - QUICK INSTALLATION
----------------------

The proposed example use a protected site named test.example.com. Non
authenticated users are redirected to auth.example.com.

1.1 - PREREQ
------------

1.1.1 - Software

To use Lemonldap::NG, you have to run a LDAP server and of course an Apache
server compiled with mod-perl (version 1.3 or 2.x). Generaly, the version of
Apache proposed with your Linux distribution match, but some distributions used
an experimental version of mod_perl with Apache2 (mod_perl-1.99) which does
not work with Lemonldap::NG. With such distributions (like Debian-3.1), you
have to use Apache-1.3 or to use a mod_perl backport (www.backports.org
package for Debian works fine).

1.1.2 - Perl prereq

Perl modules:
  Apache::Session, Net::LDAP, MIME::Base64, CGI, LWP::UserAgent, Cache::Cache,
  DBI, XML::Simple, SOAP::Lite, HTML::Template, XML::LibXML, XML::LibXSLT

With Debian:
  apt-get install libapache-session-perl libnet-ldap-perl libcache-cache-perl \
                  libdbi-perl perl-modules libwww-perl libcache-cache-perl \
                  libxml-simple-perl libhtml-template-perl libsoap-lite-perl \
                  libxml-libxml-perl libxml-libxslt-perl

1.2 - BUILDING
--------------

1.2.1 - Complete install

  $ tar xzf lemonldap-ng-*.tar.gz
  $ cd lemonldap-ng-*
  $ make && make test
  $ sudo make install

By default, all is installed in /usr/local/lemonldap-ng except Perl libraries
which are installed in a directory included in @INC.

1.2.2 - Install on Debian

  $ tar xzf lemonldap-ng-*.tar.gz
  $ cd lemonldap-ng-*
  $ debuild
  $ sudo dpkg -i ../*lemonldap-ng*.deb

Here, all is installed in /var/lib/lemonldap-ng, /etc/lemonldap-ng except Perl
libraries which are installed in /usr/share/perl5/Lemonldap/NG/

1.3 - EXAMPLE CONFIGURATION
---------------------------

If you have build Debian packages, configuration is done by Debconf. See
/usr/share/doc/liblemonldap-ng-common/README.Debian to use it.

After build, you have a new file named example/apache.conf. You just have to
include this file in Apache configuration:

  # in httpd.conf (with Apache1)
  include /path/to/lemonldap-ng/source/example/apache.conf
  # or in apache2.conf (with Apache2)
  include /path/to/lemonldap-ng/source/example/apache2.conf

Modify your /etc/hosts file to include:

  127.0.0.1 auth.example.com test1.example.com manager.example.com test2.example.com

Use a browser to connect to http://manager.example.com/ and specify your LDAP
settings. If you don't set managerDn and managerPassword, Lemonldap::NG will
use an anonymous bind to find user dn.

Next, restart Apache use your prefered browser and try to connect to
http://test1.example.com/. You'll be redirect to auth.example.com. Try
to authenticate yourself with a valid account and the protected page will
appear. You will find other explanations on this page.

the file /usr/local/lemonldap-ng/etc/storage.conf
(/etc/lemonldap-ng/storage.conf on Debian systems) can be modified to change
configuration database.

-------------------------
2 - ADVANCED INSTALLATION
-------------------------

It is recommended to install the example first then to adapt it.

2.1 - PREREQ

2.1.1 - Apache

To use Lemonldap::NG, you have to run a LDAP server and of course an Apache
server compiled with mod-perl (version 1.3 or 2.x). Generaly, the version of
Apache proposed with your Linux distribution match, but some distributions used
an experimental version of mod_perl with Apache2 (mod_perl-1.99) which does
not work with Lemonldap::NG. With such distributions (like Debian-3.1), you
have to use Apache-1.3 or to use a mod_perl backport (www.backports.org
package for Debian works fine).

For Apache2, you can use both mpm-worker and mpm-prefork. Mpm-worker works
faster and Lemonldap::NG use the thread system for best performance. If you
have to use mpm-prefork (for example if you use PHP), Lemonldap::NG will work
anyway.

You can use Lemonldap::NG in an heterogene world: the authentication portal and
the manager can work in any version of Apache 1.3 or more even if mod_perl is
not compiled, with ModPerl::Registry or not... Only the handler (site protector)
need mod_perl. The different handlers can run on different servers with
different versions of Apache/mod_perl.

2.1.2 - Perl prereq

Warning: Handler and Portal parts both need Lemonldap::NG::Manager components
to access to configuration.

Manager:
--------
Apache::Session, MIME::Base64, CGI, LWP::UserAgent, DBI, XML::Simple,
SOAP::Lite, XML::LibXML, XML::LibXSLT, Lemonldap::NG::Common

With Debian:
  apt-get install perl-modules libxml-simple-perl libdbi-perl libwww-perl
  # If you want to use SOAP
  apt-get install libsoap-lite-perl

Portal:
-------
Apache::Session, Net::LDAP, MIME::Base64, CGI, Cache::Cache, DBI, XML::Simple,
SOAP::Lite, HTML::Template, XML::LibXML, Lemonldap::NG::Common

With Debian:
  apt-get install libapache-session-perl libnet-ldap-perl perl-modules

Handler:
--------
Apache::Session, MIME::Base64, CGI, LWP::UserAgent, Cache::Cache, DBI,
XML::Simple, SOAP::Lite, Lemonldap::NG::Common

With Debian:
  apt-get install libapache-session-perl libwww-perl libcache-cache-perl

2.2 - SOFTWARE INSTALLATION
---------------------------

If you just want to install a handler or a portal or a manager:

  $ tar xzf lemonldap-ng-*.tar.gz
  $ cd lemonldap-ng-*/Lemonldap-NG-(Portal|Handler|Manager)
  $ perl Makefile.PL && make && make test
  $ sudo make install

else for a complete install:

  $ tar xzf lemonldap-ng-*.tar.gz
  $ cd lemonldap-ng-*
  $ make && make test
  $ sudo make install

See prereq in §1.1.2

2.3 - LEMONLDAP::NG INSTALLATION
--------------------------------

2.3.1 - Database configuration

2.3.1.1 - Lemonldap::NG Configuration database

If you use DBI or another system to share Lemonldap::NG configuration, you have
to initialize the database. An example is given in example/lmConfig.mysql for
MySQL.

2.3.1.2 - Apache::Session database

The choice of Apache::Session::* module is free. See Apache::Session::Store::*
or Apache::Session::* to know how to configure the module. For example, if you
want to use Apache::Session::MySQL, you can create the database like this:

  CREATE DATABASE sessions (
    id char(32),
    a_session text
  );

2.3.2 - Manager configuration

Copy example/manager.cgi and personalize it if you want (see
Lemonldap::NG::Manager). You have to set in particular configStorage. For
example with MySQL:

  $my $manager = Lemonldap::NG::Manager->new ( {
                        dbiChain   => "DBI:mysql:database=mybase;host=1.2.3.4",
                        dbiUser    => "lemonldap-ng",
                        dbiPassword => "mypass",
                 } );

Securise Manager access with Apache: Lemonldap::NG does not securise the manager
itself yet:

  SSLEngine On
  Order Deny, Allow
  Deny from all
  Allow from admin-network/netmask
  AuthType Basic
  ...

After configuration, you can also protect the manager with an Lemonldap::NG
handler.

2.3.3 - Configuration edition

Connect to the manager with your browser start configure your Web-SSO. You have
to set at least some parameters:

a) General parameters :

 * Authentication parameters -> portal : URL to access to the authentication
                                         portal
 * Domain : the cookie domain. All protected VirtualHosts have to be under it

 * LDAP parameters -> LDAP Server

 * LDAP parameters -> LDAP Accout and password : required only if anonymous
                                                 binds are not accepted

 * Session Storage -> Apache::Session module : how to store user sessions.
                                               You can use all module that
                                               inherit from Apache::Session
                                               like Apache::Session::MySQL

 * Session Storage -> Apache::Session Module parameters :
                                        see Apache::Session::<Choosen module>

b) User groups :

Use the "New Group" button to add your first group. On the left, set the
keyword which will be used later and set on the right the corresponding rule:
you can use :

 * an LDAP filter (it will be tested with the user uid)

or

 * a Perl condition enclosed with {}. All variables declared in "General
   parameters -> LDAP attributes" can be used with a "$". For example:
   MyGroup  /  { $uid eq "foo" or $uid eq "bar" }

c) Virtual hosts

You have to create a virtual host for each Apache host (virtual or real)
protected by Lemonldap::NG even if just a sub-directory is protected. Else,
user who want to access to the protected area will be rejected with a "500
Internal Server Error" message and the apache logs will explain the problem.

Each virtual host has 2 groups of parameters:

 * Headers: the headers added to the apache request. Default :
            Auth-User => $uid
 * Rules: subdivised in 2 categories:
          * default : the default rule
          * personalized rules: association of a Perl regular expression and
                                a condition. For example:
                                ^/restricted.*$  /  $groups =~ /\bMyGroup\b/


-------------
3 - DEBUGGING
-------------

Lemonldap::NG uses simply the Apache log system. So use LogLevel to choose
information to display.

